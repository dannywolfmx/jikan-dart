import 'package:test/test.dart';

import 'package:jikan/jikan.dart';

void main() {
  final instance = Jikan();
  final int ANIME_ID =
      36474; // ID for the anime "Sword Art Online: Alicization"

  final String query = "Grand Blue"; //Anime name
  final String type = 'anime';
  final int page = 1;

  test('Anime request', () async {
    Anime anime = await instance.getAnime(id: ANIME_ID);
    expect(anime, isNotNull);
    expect(anime.malId, isNotNull);
  });

  test('Schedule request', () async {
    Schedule schedule = await instance.getSchedule();
    expect(schedule, isNotNull);
    List<Anime> animes = schedule.monday;
    expect(animes, isNotEmpty);
  });

  test('Search request', () async {
    Search search =
        await instance.getSearch(type: type, query: query, page: page);
    expect(search, isNotNull);
    List<Anime> animes = search.results;
    expect(animes, isNotEmpty);
  });
}
